package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumber = new int[5];
        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;
        System.out.println("The first prime number is: " + primeNumber[0]);

        /*-----*/

        ArrayList<String> friendsArray = new ArrayList<String>() {
            {
                add("John");
                add("Jane");
                add("Chloe");
                add("Zoey");
            }
        };
        System.out.println("My friends are: " + friendsArray);

        /*-----*/

        HashMap<String, Integer> inventory = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
